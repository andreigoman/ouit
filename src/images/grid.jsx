import React from "react";

export default ({ color = "#fff", width = 14, height = 14 }) => (
  <svg
    width={width}
    height={height}
    xmlns="http://www.w3.org/2000/svg"
    viewBox={`0 0 ${16} ${16}`}
    fill={color}
  >
    <g xmlns="http://www.w3.org/2000/svg" stroke="null" id="svg_2">
      <path
        stroke="null"
        id="svg_3"
        d="m0,1.120228c0,-0.608708 0.51152,-1.120228 1.120228,-1.120228l5.121042,0c0.608708,0 1.120228,0.51152 1.120228,1.120228l0,5.121042c0,0.608708 -0.51152,1.120228 -1.120228,1.120228l-5.121042,0c-0.608708,0 -1.120228,-0.51152 -1.120228,-1.120228l0,-5.121042z"
        stroke-opacity="null"
        stroke-linecap="null"
        stroke-linejoin="null"
        stroke-width="null"
        fill-opacity="null"
      />
      <path
        stroke="null"
        id="svg_4"
        d="m0,9.761987c0,-0.608708 0.51152,-1.120228 1.120228,-1.120228l5.121042,0c0.608708,0 1.120228,0.51152 1.120228,1.120228l0,5.121042c0,0.608708 -0.51152,1.120228 -1.120228,1.120228l-5.121042,0c-0.608708,0 -1.120228,-0.51152 -1.120228,-1.120228l0,-5.121042z"
        stroke-opacity="null"
        stroke-linecap="null"
        stroke-linejoin="null"
        stroke-width="null"
        fill-opacity="null"
      />
      <path
        stroke="null"
        id="svg_5"
        d="m8.641759,1.120228c0,-0.608708 0.51152,-1.120228 1.120228,-1.120228l5.121042,0c0.608708,0 1.120228,0.51152 1.120228,1.120228l0,5.121042c0,0.608708 -0.51152,1.120228 -1.120228,1.120228l-5.121042,0c-0.608708,0 -1.120228,-0.51152 -1.120228,-1.120228l0,-5.121042z"
        stroke-opacity="null"
        stroke-linecap="null"
        stroke-linejoin="null"
        stroke-width="null"
        fill-opacity="null"
      />
      <path
        stroke="null"
        id="svg_6"
        d="m8.641759,9.761987c0,-0.608708 0.51152,-1.120228 1.120228,-1.120228l5.121042,0c0.608708,0 1.120228,0.51152 1.120228,1.120228l0,5.121042c0,0.608708 -0.51152,1.120228 -1.120228,1.120228l-5.121042,0c-0.608708,0 -1.120228,-0.51152 -1.120228,-1.120228l0,-5.121042z"
        stroke-opacity="null"
        stroke-linecap="null"
        stroke-linejoin="null"
        stroke-width="null"
        fill-opacity="null"
      />
    </g>
  </svg>
);
