import React from "react";
import { ReactComponent as Regions } from './map.svg';

import './index.css';

class RegionMap extends React.Component {
  componentDidMount() {
    if (this.props.onClickRegion) {
      for (let region of this.rootElement.querySelectorAll('.region')) {
        region.addEventListener('click', this.props.onClickRegion);
      }
    }
  }
  render = () => {
    return <div className="region-map"><Regions ref={ref => this.rootElement = ref} /></div>;
  };
}

export default RegionMap;