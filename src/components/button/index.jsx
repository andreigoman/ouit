import React from "react";

import "./styles.scss";

export default ({ children, ...restProps }) => {
  return (
    <div className="button" role="button" {...restProps}>
      <span>{children}</span>
    </div>
  );
};
