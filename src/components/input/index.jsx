import React from "react";

import "./styles.scss";

import Checkbox from "./checkbox";
export { Checkbox };
export default ({ className = "", ...restProps }) => {
  return <input type="text" className={`input ${className}`} {...restProps} />;
};
