import React from "react";

export default ({ label, className, ...restProps }) => {
  return (
    <label className="checkbox-container">
      {label}
      <input className={`checkbox ${className}`} type="checkbox" />
      <span className="checkbox-checkmark" />
    </label>
  );
};
