
import React, { createRef } from 'react';
import L from 'leaflet';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import shp from 'shpjs';

import iconUrl from 'leaflet/dist/images/marker-icon.png';
import shadowUrl from 'leaflet/dist/images/marker-shadow.png';
import iconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png';

//L.Marker.prototype.options.iconUrl = iconUrl;
//L.Marker.prototype.options.shadowUrl = shadowUrl;
//L.Marker.prototype.options.iconRetinaUrl = iconRetinaUrl;

L.Marker.prototype.options.icon = L.icon({
  iconUrl,
  shadowUrl,
  iconRetinaUrl,
  iconSize:    [25, 41],
  iconAnchor:  [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize:  [41, 41]
});

class CustomMap extends React.Component {
  state = {
    zoom: 13,
    lat: 50.00,
    lng: 30.00,
  };

  mapRef = createRef();


  componentDidMount() {
    const { shapeUrl, colors } = this.props;
    this.map = this.mapRef.current.leafletElement;
    this.renderDistricts();
    this.emitters = L.geoJSON([]);
    this.infAssortment = L.geoJSON([]);
    this.infDisposal = L.geoJSON([]);
    this.infPlacement = L.geoJSON([]);
    this.infTransfer = L.geoJSON([]);
    this.routes = L.geoJSON([], {
      style: { color: 'green' }
    });
    this.containerGrounds = L.geoJSON([]);
    this.dumps = L.geoJSON([]);
    this.perspectiveGrounds = L.geoJSON([]);
    L.control.layers(null, {
      'Источники отходов': this.emitters,
      'Объекты инфраструктуры - сортировки': this.infAssortment,
      'Объекты инфраструктуры - обезвреживание': this.infDisposal,
      'Объекты инфраструктуры – размещение': this.infPlacement,
      'Объекты инфраструктуры - размещение': this.infTransfer,
      'Маршруты мусоровозов второго и последующих звеньев': this.routes,

      'Контейнерные площадки': this.containerGrounds,
      'Несанкционированные свалки': this.dumps,
      'Перспективные площадки': this.perspectiveGrounds
    }).addTo(this.map);


    shp('/infs-assortment.zip').then(data => {
      this.infAssortment.addData(data);
    });
    shp('/infs-disposal.zip').then(data => {
      this.infDisposal.addData(data);
    });
    shp('/infs-transfer.zip').then(data => {
      this.infTransfer.addData(data);
    });
    shp('/infs-placement.zip').then(data => {
      this.infPlacement.addData(data);
    });
    shp('/routes.zip').then(data => {
      this.routes.addData(data);
    });
    shp('/dumps.zip').then(data => {
      this.dumps.addData(data);
    });
  }

  componentDidUpdate(prevProps) {
    const { shapeUrl, colors } = this.props;
    if (shapeUrl !== prevProps.shapeUrl) {
      if (this.regionLayer) {
        this.map.removeLayer(this.regionLayer);
        this.regionLayer = null;
        this.renderDistricts();
      }
    }
    if (colors !== prevProps.colors && this.regionLayer) {
      this.updateDistrictColors();
    }


  }

  updateDistrictColors = () => {
    const { colors } = this.props;
    this.regionLayer.resetStyle();
    for (let color in colors) {
      this.regionLayer.eachLayer(layer => {
        if (colors[color].includes(layer.feature.properties.NAME)) {
          layer.setStyle({ color });
          layer.bringToFront();
        }
      });
    }
  };

  renderDistricts = () => {
    if (this.props.shapeUrl) {
      if (!this.regionLayer) {
        shp(this.props.shapeUrl).then(data => {
          this.regionLayer = L.geoJson(data, {
            onEachFeature: (feature, layer) => {
              layer.bindPopup(feature.properties.NAME);
            }
          });
          this.mapRef.current.leafletElement.addLayer(this.regionLayer);
          //const center = this.regionLayer.getBounds().getCenter();
          this.map.fitBounds(this.regionLayer.getBounds());
          if (this.props.colors) {
            this.updateDistrictColors();
          }
        });
      }
    }
  }

  render() {
    const { className } = this.props;
    const { lat, lng } = this.state;
    const position = [lat, lng];
    return (
        <Map ref={this.mapRef} center={position} zoom={this.state.zoom} className={className}>
          <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          />
        </Map>
    )
  }
}

CustomMap.defaultProps = {
};

export default CustomMap