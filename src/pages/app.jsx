import React from "react";

import "./app.css";

import Main from "./main";
import Login from "./login";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    let isAuthorized = localStorage.getItem("isAuthorized") === "1";
    const lastAuthTime = parseInt(localStorage.getItem("lastAuthTime")) || 0;
    // Разлогин через 2 часа
    const isEndedSession = new Date().getTime() - lastAuthTime > 7200000;
    if (isEndedSession) {
      isAuthorized = false;
    }
    this.state = {
      isAuthorized
    };
  }

  onLogin = user => {
    localStorage.setItem("isAuthorized", "1");
    localStorage.setItem("lastAuthTime", `${new Date().getTime()}`);
    this.setState({ isAuthorized: true });
  };

  onLogout = () => {
    localStorage.setItem("isAuthorized", "0");
    localStorage.setItem("lastAuthTime", "0");
    this.setState({ isAuthorized: false });
  };

  render = () => {
    const { isAuthorized } = this.state;
    return (
      <div className="app">
        {isAuthorized ? (
          <Main onLogout={this.onLogout} />
        ) : (
          <Login onLogin={this.onLogin} />
        )}
      </div>
    );
  };
}
