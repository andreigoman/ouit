import React from "react";

import "./styles.scss";

import icon from "./app-icon.png";

import Input, { Checkbox } from "../../components/input";
import Button from "../../components/button";

export default class Login extends React.Component {
  render = () => {
    const { onLogin } = this.props;
    return (
      <div className="login-page">
        <div className="login">
          <div className="login-caption">
            <img src={icon} alt="" /> ЕГИС "УОИТ"
          </div>
          <div className="login-form">
            <Input className="user-login" value="admin" />
            <Input className="user-password" value="admin" type="password" />
            <div className="remeber-me">
              <Checkbox label="Rememer Me" />
            </div>
            <Button
              onClick={() => onLogin({ username: "admin", password: "admin" })}
            >
              Submit
            </Button>
          </div>
        </div>
      </div>
    );
  };
}
