import React from "react";

import "./styles.scss";
import RegionMap from "../../components/region-map";

export default class RegionsPage extends React.Component {
  render = () => {
    return (
      <div className="regions-page">
        <RegionMap onClickRegion={this.props.onClickRegion} />
        <div className="regions-page-footer">
          Загружены данные только по Курской области
        </div>
      </div>
    );
  };
}
