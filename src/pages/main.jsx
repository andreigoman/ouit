import React from "react";

import "./main.scss";
import Menu from "./menu";
// pages
import DefectPage from "./defect";
import RecommendationPage from "./recommendation";
import RegionsPage from "./regions";

export default class Main extends React.Component {
  state = {
    currentPage: "regions"
  };

  onPageChange = pageId => {
    this.setState({ currentPage: pageId });
  };

  renderContent = () => {
    const { currentPage } = this.state;
    switch (currentPage) {
      case "defect": {
        return <DefectPage />;
      }
      case "recommendation": {
        return <RecommendationPage />;
      }
      case "regions": {
        return <RegionsPage onClickRegion={() => this.onPageChange('defect')}/>;
      }
      default:
        return null;
    }
  };

  render = () => {
    const { currentPage } = this.state;
    const { onLogout } = this.props;
    return (
      <div className="page">
        <Menu
          currentPage={currentPage}
          onPageChange={this.onPageChange}
          onLogout={onLogout}
        />
        <div className="content">{this.renderContent()}</div>
      </div>
    );
  };
}
