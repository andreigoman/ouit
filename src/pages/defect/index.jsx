import React from "react";

import "./styles.scss";
import "leaflet/dist/leaflet.css";
import DistrictMap from '../../components/district-map';

export default class DefectPage extends React.Component {
  render = () => {
    return (
      <React.Fragment>
        <div>
        </div>
        <DistrictMap
          shapeUrl="/regions.dbf.zip"
          className="map"
          colors={{'#f00': ['Льгов']}}
        />
      </React.Fragment>
    );
  };
}
