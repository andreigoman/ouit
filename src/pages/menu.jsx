import React from "react";

import AppIcon from "../images/menu-icon.png";
import GridIcon from "../images/grid";
import LogoutIcon from "../images/logout";

const MENU_ITEMS = [
  {
    id: "regions",
    title: "Выбор региона"
  },
  {
    id: "defect",
    title: "Выявление недостатков территориальных схем"
  },
  {
    id: "recommendation",
    title: "Формирование рекомендаций"
  }
];

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderMenuItem = item => {
    const { onPageChange } = this.props;
    return (
      <div
        className="menu-item hover"
        key={item.id}
        onClick={() => onPageChange(item.id)}
      >
        <span>{item.title}</span>
      </div>
    );
  };

  render = () => {
    const { onLogout } = this.props;
    return (
      <header className="menu">
        <div className="menu-group">
          <div className="menu-item">
            <img src={AppIcon} alt="" width="24px" height="24px" />
          </div>
          {MENU_ITEMS.map(this.renderMenuItem)}
        </div>
        <div className="menu-group">
          <div className="menu-item">administrator[admin]</div>
          <div className="menu-item" title="Новое окно">
            <GridIcon color="#bdbdbd" />
          </div>
          <div className="menu-item" title="Выход" onClick={onLogout}>
            <LogoutIcon color="#bdbdbd" />
          </div>
        </div>
      </header>
    );
  };
}
